Configuration dump of the wireless routers that are incapable of being configured as code and forces everything through a UI.

[[_TOC_]]


# TP-Link Archer C7 v2 (AC1750)


## Status

N/A


## Quick Setup

N/A


## Network


### WAN

N/A


### LAN

    MAC Address: C4-E9-84-C2-0B-62
    IP Address:  192.168.2.2
    Subnet Mask: 255.255.255.0
    IGMP Proxy:  Enable


### MAC Clone

N/A


## Dual Band Selection

    Concurrently with 2.4GHz and 5GHz (802.11a/b/g/n/ac)


## Wireless 2.4GHz


### Wireless Settings

    Wireless Network Name: RockOnNetwork
    Region:                Canada
    Mode:                  11bgn mixed
    Channel Width:         Auto
    Channel:               Auto
    Enable SSID Broadcast: true
    Enable WDS Bridging:   false


### WPS

Disabled


### Wireless Security

    WPA/WPA2 - Personal
    Version:      Automatic
    Encryption:   AES
    PSK Password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      33666563666566343239656335666439633630663536666564666139656162316161396165653931
      3935633164386266343034346634323634646132346162620a623033363762383739373136363763
      34623062393132343061626634663163663066613163353731316436663838303838346265613030
      3231326366346339380a323334356666396365333233343933646237313934343533303038353239
      64373231616561373836326565363963306439303937646234666632363936396638
    Group Key Update Period: 0


### Wireless MAC Filtering

Disabled


### Wireless Advanced
    Transmit Power:          High
    Beacon Interval:         100
    RTS Threshold:           2346
    Fragmentation Threshold: 2346
    DTIM Interval:           1
    Enable WMM:              true
    Enable Short GI:         true
    Enable AP Isolation:     false


### Wireless Statistics

N/A


## Wireless 5GHz


### Wireless Settings

    Wireless Network Name: RockOnNetwork_Media
    Region:                Canada
    Mode:                  11a/n/ac mixed
    Channel:               Auto
    Enable SSID Broadcast: true
    Enable WDS Bridging:   false


### WPS

Disabled


### Wireless Security

    WPA/WPA2 - Personal
    Version:      Automatic
    Encryption:   AES
    PSK Password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      33666563666566343239656335666439633630663536666564666139656162316161396165653931
      3935633164386266343034346634323634646132346162620a623033363762383739373136363763
      34623062393132343061626634663163663066613163353731316436663838303838346265613030
      3231326366346339380a323334356666396365333233343933646237313934343533303038353239
      64373231616561373836326565363963306439303937646234666632363936396638
    Group Key Update Period: 0


### Wireless MAC Filtering

Disabled


### Wireless Advanced
    Transmit Power:          High
    Beacon Interval:         100
    RTS Threshold:           2346
    Fragmentation Threshold: 2346
    DTIM Interval:           1
    Enable WMM:              true
    Enable Short GI:         true
    Enable AP Isolation:     false


### Wireless Statistics

N/A


## Guest Network


### Wireless Settings

    Allow Guest To Access My Local Network: false
    Enable Guest Network Bandwidth Control: false
    Guest Network (2.4G):                   false
    Guest Network (5G):                     false


### Storage Sharing

Stopped


## DHCP


### DHCP Settings

    DHCP Server: Disable


### DHCP Clients List

N/A


### Address Reservation

N/A


## USB Settings


### Storage Sharing

Stopped


### FTP Server

Stopped


### Media Server

Stopped


### Print Server

Offline


### User Accounts

N/A


## NAT

    Current NAT Status:          Enable
    Current Hardware NAT Status: Enable


## Forwarding


### Virtual Servers

N/A


### Port Triggering

N/A


### DMZ

    Current DMZ Status: Disable


### UPnP

N/A


## Security


### Basic Security

All enabled


### Advanced Security

All defaults


### Local Management

All


### Remote Management

    Web Management Port:          80
    Remote Management IP Address: 0.0.0.0


## Parental Control

Disable


## Access Control

Not enabled


## Advanced Routing

N/A


## Bandwidth Control

N/A


## IP & MAC Binding

N/A


## Dynamic DNS

N/A


## IPv6 Support

N/A
