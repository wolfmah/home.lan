#!/bin/sh

#------------------------------------------------------------------------------
# Need to be run as root.
#
# OS: Debian 10.10.0 (Proxy for Raspbian)
# Note: Should we try Alpine or straight up Debian on Arm?
#
# - Delete unnecessary user created during install.
# - Setup a minimal `ssh` with root password allowed.
# - Setup `sudo` for the user `cm`.
# - Setup `ansible` in a Python virtual environment.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Non graphical install
# Select a language: English
# Select your location: Canada
# Configure the keyboard: American English
# Hostname: cm {should already be filled in}
# Domain name: home.lan {should already be filled in}
# Root password: -----
# Set up users and passwords: test
# User password: -----
# Time zone: Eastern
# Partitioning method: Guided - use entire disk
# Select disk to partition: sda
# Partitioning scheme: All files in one partition
# Finish partitioning and write changes to disk
# Scan another CD: no
# Debian archive mirror country: Canada
# Debian archive mirror: whatever
# Proxy: none
# Participate in the package usage survey: no
# Software selection: none
# Install the GRUB boot loader to the master boot record: yes
# Device for boot loader installation: /dev/sda
# Reboot
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# apt update
# apt install -y curl
# /bin/sh -c "$(curl -s https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/cm-install.sh)"
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
cat << EOF > /root/bootstrap.sh
set -x
apt update
apt install -y curl python3
curl -o /tmp/cm.py https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/cm.py
curl -o /tmp/common.py https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/common.py
chmod +x /tmp/cm.py
/tmp/cm.py ${1:-vm}
if [ $? -eq 0 ]; then /tmp/template.sh; fi
EOF

chmod +x /root/bootstrap.sh
/root/bootstrap.sh $@

# When completed, manually change /home/cm/home.lan/ansible/.vault-pass value.
#------------------------------------------------------------------------------
