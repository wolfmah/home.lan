#!/bin/sh -eux

ENV_DNS_FORWARDING=__ENV_DNS_FORWARDING__
if [ "$ENV_DNS_FORWARDING" = true ] ; then
    touch /etc/ENV_DNS_FORWARDING
fi

CM_PASSWORD=__CM_PASSWORD__
CM_USER=__CM_USER__
DNS_ACCESS_CONTROL_NETBLOCK=__DNS_ACCESS_CONTROL_NETBLOCK__
DNS_MSG_CACHE_SIZE=__DNS_MSG_CACHE_SIZE__
DNS_NUMBER_SLABS=__DNS_NUMBER_SLABS__
DNS_NUMBER_THREADS=__DNS_NUMBER_THREADS__
DNS_RRSET_CACHE_SIZE=__DNS_RRSET_CACHE_SIZE__
DOMAIN_FQDN=__DOMAIN_FQDN__
DOMAIN_HOSTNAME=__DOMAIN_HOSTNAME__
DOMAIN_NAME=__DOMAIN_NAME__
ENVIRONMENT=__ENVIRONMENT__
FORGE_BASE_URL=__FORGE_BASE_URL__
HOSTNAME_CM=__HOSTNAME_CM__
HOSTNAME_GATEWAY=__HOSTNAME_GATEWAY__
IP_ADDRESS_CM=__IP_ADDRESS_CM__
IP_ADDRESS_GATEWAY_1=__IP_ADDRESS_GATEWAY_1__
IP_ADDRESS_GATEWAY_2=__IP_ADDRESS_GATEWAY_2__
IP_ADDRESS_GATEWAY_3=__IP_ADDRESS_GATEWAY_3__
IP_ADDRESS_GATEWAY_4=__IP_ADDRESS_GATEWAY_4__
MAC_ADDRESS_CM=__MAC_ADDRESS_CM__
MAC_ADDRESS_GATEWAY_1=__MAC_ADDRESS_GATEWAY_1__
MAC_ADDRESS_GATEWAY_2=__MAC_ADDRESS_GATEWAY_2__
MAC_ADDRESS_GATEWAY_3=__MAC_ADDRESS_GATEWAY_3__
MAC_ADDRESS_GATEWAY_4=__MAC_ADDRESS_GATEWAY_4__
NIC_INTERFACE_NAME_1=__NIC_INTERFACE_NAME_1__
NIC_INTERFACE_NAME_2=__NIC_INTERFACE_NAME_2__
NIC_INTERFACE_NAME_3=__NIC_INTERFACE_NAME_3__
NIC_INTERFACE_NAME_4=__NIC_INTERFACE_NAME_4__
SSH_ALLOW_USERS=__SSH_ALLOW_USERS__

####################
# Hostname
echo "${DOMAIN_FQDN}" > /etc/myname
hostname ${DOMAIN_FQDN}

####################
# Update the system.
pkg_add -u

####################
# Install a few basic tools.
# pkg_add -I curl nano python%3.7 py3-pip
pkg_add -I curl nano python%3.8 py3-pip

####################
# Create symlinks, since most scripts expect python3 to have also a major-version-only of their binaries.
# ln -sf /usr/local/bin/2to3-3.7 /usr/local/bin/2to3
# ln -sf /usr/local/bin/easy_install-3.7 /usr/local/bin/easy_install-3
# ln -sf /usr/local/bin/pip3.7 /usr/local/bin/pip3
# ln -sf /usr/local/bin/python3.7 /usr/local/bin/python3
# ln -sf /usr/local/bin/python3.7m /usr/local/bin/python3m
# ln -sf /usr/local/bin/python3.7m-config /usr/local/bin/python3m-config
# ln -sf /usr/local/bin/pyvenv-3.7 /usr/local/bin/pyvenv-3
# pip3 install --upgrade pip
ln -sf /usr/local/bin/pip3.8 /usr/local/bin/pip3
pip3 install --upgrade pip

####################
# Setup the default user password and ensure the shell is `ksh`.
curl -o /etc/adduser.conf ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/adduser.conf
chown root:wheel /etc/adduser.conf
chmod 644 /etc/adduser.conf

groupadd ${CM_USER}
groupadd ssh-users
adduser -batch ${CM_USER} ${CM_USER},wheel,ssh-users ${CM_USER} ${CM_PASSWORD}
chsh -s ksh ${CM_USER}

####################
# Enable `doas` for ${CM_USER}
echo "permit nopass ${CM_USER} as root" > /etc/doas.conf

####################
# Create an authorized keys file and insert the insecure public key.
mkdir -pm 0700 /home/${CM_USER}/.ssh
curl ${FORGE_BASE_URL}/bootstrap/files/ssh/cm--cm--gateway.pub >> /home/${CM_USER}/.ssh/authorized_keys
chmod 0600 /home/${CM_USER}/.ssh/authorized_keys
chown -R ${CM_USER}:${CM_USER} /home/${CM_USER}/.ssh

####################
# Enable minimal ssh
rcctl enable sshd
curl -o /etc/ssh/sshd_config ${FORGE_BASE_URL}/bootstrap/files/ssh/sshd_config
chown root:wheel /etc/ssh/sshd_config
chmod 644 /etc/ssh/sshd_config
sed -i "s/--SSH_ALLOW_USERS--/${SSH_ALLOW_USERS}/g" /etc/ssh/sshd_config

####################
# Ensure network is OK
echo "net.inet.ip.forwarding=1" >> /etc/sysctl.conf
chown root:wheel /etc/sysctl.conf
chmod 644 /etc/sysctl.conf

echo "${IP_ADDRESS_GATEWAY_1}" > /etc/hostname.${NIC_INTERFACE_NAME_1}
chown root:wheel /etc/hostname.${NIC_INTERFACE_NAME_1}
chmod 640 /etc/hostname.${NIC_INTERFACE_NAME_1}
echo "inet ${IP_ADDRESS_GATEWAY_2} 255.255.255.0" > /etc/hostname.${NIC_INTERFACE_NAME_2}
chown root:wheel /etc/hostname.${NIC_INTERFACE_NAME_2}
chmod 640 /etc/hostname.${NIC_INTERFACE_NAME_2}
echo "inet ${IP_ADDRESS_GATEWAY_3} 255.255.255.0" > /etc/hostname.${NIC_INTERFACE_NAME_3}
chown root:wheel /etc/hostname.${NIC_INTERFACE_NAME_3}
chmod 640 /etc/hostname.${NIC_INTERFACE_NAME_3}
echo "inet ${IP_ADDRESS_GATEWAY_4} 255.255.255.0" > /etc/hostname.${NIC_INTERFACE_NAME_4}
chown root:wheel /etc/hostname.${NIC_INTERFACE_NAME_4}
chmod 640 /etc/hostname.${NIC_INTERFACE_NAME_4}

####################
# DHCP
rcctl enable dhcpd
rcctl set dhcpd flags ${NIC_INTERFACE_NAME_2} ${NIC_INTERFACE_NAME_3} ${NIC_INTERFACE_NAME_4}
curl -o /etc/dhcpd.conf ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/dhcpd.conf
chown root:wheel /etc/dhcpd.conf
chmod 644 /etc/dhcpd.conf
sed -i "s/--DOMAIN_NAME--/${DOMAIN_NAME}/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_GATEWAY_2--/${IP_ADDRESS_GATEWAY_2}/g" /etc/dhcpd.conf
sed -i "s/--IP_ADDRESS_CM--/${IP_ADDRESS_CM}/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_GATEWAY_2--/${MAC_ADDRESS_GATEWAY_2}/g" /etc/dhcpd.conf
sed -i "s/--MAC_ADDRESS_CM--/${MAC_ADDRESS_CM}/g" /etc/dhcpd.conf

curl -o /etc/dhclient.conf ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/dhclient.conf
chown root:wheel /etc/dhclient.conf
chmod 644 /etc/dhclient.conf
sed -i "s/--NIC_INTERFACE_NAME_1--/${NIC_INTERFACE_NAME_1}/g" /etc/dhclient.conf

####################
# Firewall/NAT
rcctl enable pf
curl -o /etc/pf.conf ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/pf.conf
chown root:wheel /etc/pf.conf
chmod 600 /etc/pf.conf
sed -i "s/--NIC_INTERFACE_NAME_2--/${NIC_INTERFACE_NAME_2}/g" /etc/pf.conf

####################
# DNS
rcctl enable unbound

curl -o /var/unbound/etc/root.hints ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/root.hints
chown root:wheel /var/unbound/etc/root.hints
chmod 644 /var/unbound/etc/root.hints

if [ ! -f /etc/ENV_DNS_FORWARDING ]; then
    curl -o /var/unbound/db/root.key ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/root.key
    chown root:_unbound /var/unbound/db/root.key
    chmod 664 /var/unbound/db/root.key
fi

if [ ! -f /etc/ENV_DNS_FORWARDING ]; then
    curl -o /var/unbound/etc/unbound.conf ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/unbound.conf
else
    curl -o /var/unbound/etc/unbound.conf ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/unbound_ENV_DNS_FORWARDING.conf
fi
chown root:wheel /var/unbound/etc/unbound.conf
chmod 644 /var/unbound/etc/unbound.conf
sed -i "s/--DNS_NUMBER_THREADS--/${DNS_NUMBER_THREADS}/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_NUMBER_SLABS--/${DNS_NUMBER_SLABS}/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_RRSET_CACHE_SIZE--/${DNS_RRSET_CACHE_SIZE}/g" /var/unbound/etc/unbound.conf
sed -i "s/--DNS_MSG_CACHE_SIZE--/${DNS_MSG_CACHE_SIZE}/g" /var/unbound/etc/unbound.conf
sed -i "s/--DOMAIN_NAME--/${DOMAIN_NAME}/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_GATEWAY_1--/${IP_ADDRESS_GATEWAY_1}/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_GATEWAY_2--/${IP_ADDRESS_GATEWAY_2}/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_GATEWAY_3--/${IP_ADDRESS_GATEWAY_3}/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_GATEWAY_4--/${IP_ADDRESS_GATEWAY_4}/g" /var/unbound/etc/unbound.conf
sed -i "s/--IP_ADDRESS_CM--/${IP_ADDRESS_CM}/g" /var/unbound/etc/unbound.conf
sed -i "s|--DNS_ACCESS_CONTROL_NETBLOCK--|${DNS_ACCESS_CONTROL_NETBLOCK}|g" /var/unbound/etc/unbound.conf

####################
# Patch system with securty fixes
syspatch

####################
# Cleanup
rm -f /root/bootstrap.sh
rm -f /tmp/common.py
rm -f /tmp/${DOMAIN_HOSTNAME}.py
rm -f /tmp/template.sh
sync
reboot
