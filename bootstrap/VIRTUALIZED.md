# Virtualized environment

The virtualized environment configuration is highly dependent on the host; different host requires different configuration.

[[_TOC_]]


## Linux

I am not developing on Linux right now, though it would be the best. That's because I could use [QEMU](https://www.qemu.org) and use an actual VM emulating the Raspberry Pi used for the configuration management machine.


## macOS

I have tried real hard to make [QEMU](https://www.qemu.org) work on macOS (10.15.2, Catalina, circa January 2020) to make use of the hardware virtualization so I could emulate an actual Raspberry Pi, but I haven't been able to make it work all the way through. I was able to make the Raspberry Pi emulation possible by using [dhruvvyas90's kernel](https://github.com/dhruvvyas90/qemu-rpi-kernel):
```
qemu-system-arm \
    -name home.lan.cm \
    -kernel ".../kernel-qemu-4.19.50-buster" \
    -append 'root=/dev/sda2 panic=1 rootfstype=ext4 rw' \
    -dtb ".../versatile-pb.dtb" \
    -M versatilepb \
    -no-reboot \
    -cpu arm1176 \
    -m 256M \
    -drive "file=.../2019-09-26-raspbian-buster-lite.img,index=0,media=disk,format=raw" \
    -nic user,model=virtio-net-pci,mac=00:00:10:00:01:05 \
    -serial stdio
```

What I kept stumbling upon, was the creation of a private network. Even if [QEMU's documentation](https://www.qemu.org/2018/05/31/nic-parameter/) offer a solid understanding of the command line options. This setup is so easy to do inside Virtual Box (select the right option inside a combo box), and seems pretty straight forward for Qemu on Linux hosts (via `ip`/`ifconfig`), but on macOS, that's pretty much impossible. No documentation exists on the internet. All Linux documentation points to configuration via [`ip`](https://linux.die.net/man/8/ip), but no `ip` port exists on macOS, only `ifconfig`. No tun/tap interface, only an unmaintained project that's dead since 2015.


### VirtualBox

Whatever is not defined, leave it to the default value.


#### Gateway

|   | |
------ | ------
**Group** | `home.lan`
**Name** | `gateway`
**Type** | BSD
**Version** | OpenBSD (64-bit)
**Memory size** | 512 MB
**Hard disk** | Create a virtual hard disk now
**Audio** | Disable audio

| Network | |
------ | ------
**Adapter 1** | **Attached to**: NAT<br/>**MAC address**: 000010000001
**Adapter 2** | **Attached to**: Internal Network<br/>**Name**: LAN<br/>**Adapter type**: Intel PRO/1000 MT Desktop (82540EM)<br/>**Promiscuous mode**: Deny<br/>**MAC address**: 000010010101
**Adapter 3** | **Attached to**: Internal Network<br/>**Name**: WIFI<br/>**Adapter type**: Intel PRO/1000 MT Desktop (82540EM)<br/>**Promiscuous mode**: Deny<br/>**MAC address**: 000010020201
**Adapter 4** | **Attached to**: Internal Network<br/>**Name**: DMZ<br/>**Adapter type**: Intel PRO/1000 MT Desktop (82540EM)<br/>**Promiscuous mode**: Deny<br/>**MAC address**: 000010101001


#### Configuration management

|   | |
------ | ------
**Group** | `home.lan`
**Name** | `cm`
**Type** | Linux
**Version** | Debian (64-bit)
**Memory size** | 1024 MB
**Hard disk** | Create a virtual hard disk now
**Audio** | Disable audio

| Network | |
------ | ------
**Adapter 1** | **Attached to**: Internal Network<br/>**Name**: LAN<br/>**Adapter type**: Intel PRO/1000 MT Desktop (82540EM)<br/>**Promiscuous mode**: Deny<br/>**MAC address**: 000010010105
