#!/usr/bin/env bash
set -eux

CM_PASSWORD=__CM_PASSWORD__
CM_USER=__CM_USER__
DOMAIN_HOSTNAME=__DOMAIN_HOSTNAME__
DOMAIN_NAME=__DOMAIN_NAME__
FORGE_CLONE_URL=__FORGE_CLONE_URL__
FORGE_BASE_URL=__FORGE_BASE_URL__
HOSTNAME_GATEWAY=__HOSTNAME_GATEWAY__

if [[ `whoami` == 'root' ]]; then
    ####################
    # IF user is root

    ENV_DNS_FORWARDING=__ENV_DNS_FORWARDING__
    if [ "$ENV_DNS_FORWARDING" = true ] ; then
        touch /etc/ENV_DNS_FORWARDING
    fi

    ####################
    # Update the system.
    apt update
    apt upgrade -y
    apt install -y man curl dirmngr git less

    ####################
    # Delete test user, forced to create during install
    apt install -y perl

    # There is no pi user in Debian Raspberry Pi image.
    if [ -z "`uname -a | grep "aarch64"`" ]; then
        deluser --remove-home --remove-all-files test
        if [ ! -z `cat /etc/group | grep "^test:"` ]; then
            delgroup test
        fi
    fi

    ####################
    # Enable minimal ssh
    curl -o /etc/ssh/ssh_config ${FORGE_BASE_URL}/bootstrap/files/ssh/ssh_config
    chown root:root /etc/ssh/ssh_config
    chmod 644 /etc/ssh/ssh_config

    ####################
    # Install `sudo`
    apt install -y sudo

    ####################
    # Enable `sudo` for ${CM_USER}
    curl -o /etc/sudoers ${FORGE_BASE_URL}/bootstrap/${DOMAIN_HOSTNAME}/files/sudoers
    chown root:root /etc/sudoers
    chmod 440 /etc/sudoers

    ####################
    # Add ${CM_USER} user
    groupadd ssh-users
    useradd -m -p ${CM_PASSWORD} -c ${CM_USER} -s /bin/bash -G sudo,ssh-users ${CM_USER}

    echo ""
    echo "-------------------------------------------------------------------------------"
    echo "Done, now complete the setup by switching to ${CM_USER} user."
    echo "su - ${CM_USER}"
    echo "/tmp/template.sh"
    exit 0
elif [[ `whoami` == "${CM_USER}" ]]; then
    ####################
    # IF user is ${CM_USER}

    ANSIBLE_VENV="${HOME}/ansible"
    ANSIBLE_PATH="${ANSIBLE_VENV}/bin"
    HOME_LAN_PATH="${HOME}/home.lan"

    ####################
    # Install a few basic tools.
    sudo apt install -y git less man python3 python3-apt python-apt-doc python3-doc python3-pip python3-venv

    MY_PYTHON_VERSION=3.10.1
    PYTHON_EXEC=python3.10

    ####################
    # Install Python
    sudo apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev
    (
        cd /tmp

        curl -o Python-${MY_PYTHON_VERSION}.tgz https://www.python.org/ftp/python/${MY_PYTHON_VERSION}/Python-${MY_PYTHON_VERSION}.tgz
        tar -xf Python-${MY_PYTHON_VERSION}.tgz

        cd Python-${MY_PYTHON_VERSION}

        ./configure --enable-optimizations
        make -j $(nproc)
        sudo make altinstall
    )

    ####################
    # Install Python virtual environment, and Ansible inside it
    ${PYTHON_EXEC} -m venv ${ANSIBLE_VENV}
    (
        set +eu
        source ${ANSIBLE_VENV}/bin/activate

        pip3 install --upgrade pip
        pip3 install --upgrade passlib bcrypt cryptography jmespath
        pip3 install --upgrade ansible

        deactivate
        set -eu
    )

    ####################
    # Install `python3-apt` inside the virtual environment, otherwise, playbooks won't be able to talk to `apt` on the host.
    # Reference: https://github.com/ansible/ansible/issues/14468#issuecomment-459630445
    (
        cd /tmp
        apt download python3-apt
        dpkg -x python3-apt* python3-apt

        cp -r /tmp/python3-apt/usr/lib/python3/dist-packages/* ${ANSIBLE_VENV}/lib/python3*/site-packages/

        cd ${ANSIBLE_VENV}/lib/python3*/site-packages/
        mv apt_pkg.cpython-*-linux-gnu.so apt_pkg.so
        mv apt_inst.cpython-*-linux-gnu.so apt_inst.so
    )

    ####################
    # Create wrapper scripts to make calling Ansible executable, which are inside a virtual environment, seemless on the host system.
    mkdir -p ~/bin

    for EXEC_NAME in `ls -1 ${ANSIBLE_VENV}/bin/ansible*`; do
        EXEC_NAME=`basename ${EXEC_NAME}`
        echo '#!/usr/bin/env bash' > ~/bin/${EXEC_NAME}
        echo "set -eu" >> ~/bin/${EXEC_NAME}
        echo "source \"${ANSIBLE_VENV}/bin/activate\"" >> ~/bin/${EXEC_NAME}
        echo "${ANSIBLE_VENV}/bin/${EXEC_NAME} \$@" >> ~/bin/${EXEC_NAME}
        echo "deactivate" >> ~/bin/${EXEC_NAME}
    done
    chmod 700 ~/bin/*

    ####################
    # Add Ansible project
    cat << EOF > ${HOME}/.gitconfig
[user]
    name = Marc-Andre Harbec
    email = 1987451-wolfmah@users.noreply.gitlab.com

[pull]
    rebase = false
EOF

    git clone ${FORGE_CLONE_URL} ${HOME_LAN_PATH}
    echo "test" > ${HOME_LAN_PATH}/ansible/.vault-pass
    chmod 600 ${HOME_LAN_PATH}/ansible/.vault-pass

    ####################
    # Add SSH keys
    if [ ! -d ~/.ssh ]; then
        mkdir ~/.ssh
        chmod 700 ~/.ssh
    fi
    curl -o ~/.ssh/cm--cm--gateway ${FORGE_BASE_URL}/bootstrap/files/ssh/cm--cm--gateway
    chmod 600 ~/.ssh/cm--cm--gateway

    ####################
    # Add SSH fingerprints
    if [ ! -f ~/.ssh/known_hosts ]; then
        touch ~/.ssh/known_hosts
        chmod 600 ~/.ssh/known_hosts
    fi
    ssh-keyscan -H -t ed25519 ${HOSTNAME_GATEWAY}.${DOMAIN_NAME} >> ~/.ssh/known_hosts

    ####################
    # Cleanup
    sudo rm -rf /tmp/Python-${MY_PYTHON_VERSION} /tmp/Python-${MY_PYTHON_VERSION}.tgz
    sudo rm -rf /tmp/python3-apt*
    sudo rm -f /root/bootstrap.sh
    sudo rm -f /tmp/common.py
    sudo rm -f /tmp/${DOMAIN_HOSTNAME}.py
    sudo rm -f /tmp/template.sh
    sudo sync
    sudo reboot
fi
