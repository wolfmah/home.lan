#!/bin/sh

#------------------------------------------------------------------------------
# Need to be run as root.
#
# OS: OpenBSD 7.0
# 
# - Setup a minimal `ssh` with root password allowed.
# - Setup a minimal `pf` configuration.
# - Setup an almost complete `unbound` configuration.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Installation step-by-step
# Install
# Keyboard layout: default
# System hostname: gateway
# Which network interface...?
#   em0
#       IPv4 address: autoconf
#       IPv6 address: none
#   done
# Password for root account: -----
# Start sshd(8) by default: yes
# X Window System: no
# Setup a user: no
# Allow root ssh login: no
# What timezone: EST5EDT
# Which disk it the root disk: wd0
# Whole
# Auto
# Location of sets: cd0
# Pathname to the sets: 7.0/amd64
# Set name(s): -g*
# Set name(s): -x*
# Set name(s): done
# Continue without verification: yes
# Location of sets: done
# Reboot
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# mail --> d * --> q
# echo "https://cloudflare.cdn.openbsd.org/pub/OpenBSD" > /etc/installurl
# pkg_add curl
# /bin/sh -c "$(curl -s https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/gateway-install.sh)"
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
cat << EOF > /root/bootstrap.sh
set -x
echo "https://cloudflare.cdn.openbsd.org/pub/OpenBSD" > /etc/installurl
pkg_add curl python%3.8
curl -o /tmp/gateway.py https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/gateway.py
curl -o /tmp/common.py https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/common.py
chmod +x /tmp/gateway.py
/tmp/gateway.py ${1:-vm}
if [ $? -eq 0 ]; then /tmp/template.sh; fi
EOF

chmod +x /root/bootstrap.sh
/root/bootstrap.sh $@
#------------------------------------------------------------------------------
