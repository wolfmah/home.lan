To know the manual steps required to bootstrap the infrastructure, follow those links:
* [Virtualized environment](VIRTUALIZED.md)
* [Bare metal environment](BARE-METAL.md)


## Usage

The basic network comprise the gateway and configuration management (CM) machine. When starting from scratch (no server installed), those two machines will require special treatment to bootstrap; can't do it via Ansible, as their is no CM machine and no network.

To bootstrap those two machine, they need to be done in a specific order: gateway, than CM. The bootstrapping steps are as followed:

* Gateway
    1. Manually install the gateway. Answers to the installation questions can be found in the header comment of [`gateway.py`](gateway.py).
    1. Manually create a shell script by following the description found in the header comment of [`gateway.py`](gateway.py).
    1. Execute the shell script.
* CM
    1. Manually install the CM. Answers to the installation questions can be found in the header comment of [`cm.py`](cm.py).
    1. Manually create a shell script by following the description found in the header comment of [`cm.py`](cm.py).
    1. Execute the shell script.
    1. Log off, log in as the user `cm` and resume the execution.

Now, the bootstrapping of the two servers is done. It is now possible to provision them with appropriate configurations coming from Ansible.

1. On CM, log with user `cm` and `cd` into `~/home.lan/ansible`.
    1. Change de default, non-valid, password found in the file `~/home.lan/ansible/.vault-pass`.
    1. First, CM needs to be provisioned before it can execute proper Ansible commands: `ansible-playbook -i ./inventory/staging-networking.yml provision-cm.yml`
    1. Then, the network can be provisioned with: `ansible-playbook -i ./inventory/staging-networking.yml provision-network.yml`

With that, the most basic network of servers is configured and ready to roll. When adding new servers to the mix, update the appropriate Ansible roles/playbooks/scripts/vars and do the following (example is done with a nas/Debian host):

1. On the newly installed host, log with `root`
    1. `apt install -y openssh-server openssh-client`
    1. `echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config`
    1. `systemctl restart sshd`
1. On CM, log with user `cm` and `cd` into `~/home.lan/ansible`
    1. `ansible-playbook -i ./inventory/staging.yml provision-network.yml`
    1. `ansible-playbook -i ./inventory/staging.yml -k bootstrap-debian.yml --extra-vars="target=nas"`
    1. Provide the requested `root` password of the target.
    1. `ansible-playbook -i ./inventory/staging.yml home.lan.yml`

The bootstrapping of any other server, other than gateway and CM, can be done in any order.

The whole network can be kept maintained with: `ansible-playbook -i ./inventory/staging.yml home.lan.yml`.


## Update host

### Gateway

When a new version of OpenBSD is available, the best option is to reinstall everything and let CM reconfigure it all. Though, this is a special case, because updating the gateway cuts the internet for the whole lan.

1. Test the new version in the VM environment
1. Install the new version on the actual machine
    1. Use the same procedure found in `bootstrap/gateway-install.sh`
1. On CM, log with user `cm`
    1. Remove Gateway entries from `known_hosts`:
        1. `ssh-keygen -R 10.1.1.1`
        1. `ssh-keygen -R gateway.home.lan`
    1. Re-download the original dummy private SSH key:
        1. `curl -s https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/files/ssh/cm--cm--gateway > ~/.ssh/cm--cm--gateway`
    1. Re-scan the gateway to `known_hosts`
        1. `ssh-keyscan -H -t ed25519 10.1.1.1 >> ~/.ssh/known_hosts`
        1. `ssh-keyscan -H -t ed25519 gateway.home.lan >> ~/.ssh/known_hosts`
    1. Execute `provision-network.yml` playbook
        1. `ansible-playbook -i ./inventory/staging-networking.yml provision-network.yml`


### CM

When a new version of Debian is available, the best option is to reinstall everything and let CM reconfigure it all. Though, this is a special case.

1. Test the new version in the VM environment
1. Install the new version on the actual machine
1. Use the same procedure found in `bootstrap/cm-install.sh`
1. Don't forget about `.vault-pass`
1. On Gateway, log with user `cm`
    1. Re-authorize the original dummy private SSH key
        1. `curl -s https://gitlab.com/wolfmah/home.lan/-/raw/master/bootstrap/files/ssh/cm--cm--gateway.pub >> /home/cm/.ssh/authorized_keys`
1. On CM, log with user `cm`
    1. Remove Gateway entries from `known_hosts`:
        1. `ssh-keygen -R 10.1.1.1`
        1. `ssh-keygen -R gateway.home.lan`
    1. Re-scan the gateway to `known_hosts`
        1. `ssh-keyscan -H -t ed25519 10.1.1.1 >> ~/.ssh/known_hosts`
        1. `ssh-keyscan -H -t ed25519 gateway.home.lan >> ~/.ssh/known_hosts`
    1. Execute `provision-cm.yml` playbook
        1. `ansible-playbook -i ./inventory/staging-networking.yml provision-cm.yml`
    1. Execute `provision-network.yml` playbook
        1. `ansible-playbook -i ./inventory/staging-networking.yml provision-network.yml`
