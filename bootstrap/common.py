import argparse
import os
import re
import shutil
import tempfile
import urllib.request
import urllib.error

FORGE_CLONE_URL = "https://gitlab.com/wolfmah/home.lan.git"
FORGE_BASE_URL = "https://gitlab.com/wolfmah/home.lan/-/raw/master"

DOMAIN_SECOND_LEVEL = "home"
DOMAIN_TOP_LEVEL = "lan"
DOMAIN_NAME = "%s.%s" % (DOMAIN_SECOND_LEVEL, DOMAIN_TOP_LEVEL)

CM_USER = "cm"

IP_ADDRESS_GATEWAY_1 = "dhcp"
IP_ADDRESS_GATEWAY_2 = "10.1.1.1"
IP_ADDRESS_GATEWAY_3 = "10.2.2.1"
IP_ADDRESS_GATEWAY_4 = "10.10.10.1"
IP_ADDRESS_CM = "10.1.1.5"

HOSTNAME_GATEWAY = "gateway"
HOSTNAME_CM = "cm"

def parse_args():
    parser = argparse.ArgumentParser(description="Bootstrap GATEWAY, an OpenBSD router.")

    parser.add_argument(
        "environment",
        choices=["vm", "vm_dns_forwarding", "metal"])

    return parser.parse_args()

def download_file(url, destination_file):
    print("Downloading %s" % (url))

    user_agent = 'Mozilla/5.0 (Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0'
    headers = { 'User-Agent':user_agent }
    request = urllib.request.Request(url, None, headers)
    file = urllib.request.urlopen(request)

    with open(destination_file, "wb") as local_file:
        local_file.write(file.read())

def sed_inplace(filename, pattern, repl):
    '''
    Perform the pure-Python equivalent of in-place `sed` substitution: e.g.,
    `sed -i -e 's/'${pattern}'/'${repl}' "${filename}"`.
    '''
    # For efficiency, precompile the passed regular expression.
    pattern_compiled = re.compile(pattern)

    # For portability, NamedTemporaryFile() defaults to mode "w+b" (i.e.,
    # binary writing with updating). This is usually a good thing. In this
    # case, however, binary writing imposes non-trivial encoding constraints
    # trivially resolved by switching to text writing. Let's do that.
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as tmp_file:
        with open(filename) as src_file:
            for line in src_file:
                tmp_file.write(pattern_compiled.sub(repl, line))

    # Overwrite the original file with the munged temporary file in a manner
    # preserving file attributes (e.g., permissions).
    shutil.copystat(filename, tmp_file.name)
    shutil.move(tmp_file.name, filename)

def run(settings):
    settings["FORGE_CLONE_URL"] = FORGE_CLONE_URL
    settings["FORGE_BASE_URL"] = FORGE_BASE_URL
    settings["DOMAIN_NAME"] = DOMAIN_NAME
    settings["DOMAIN_FQDN"] = "%s.%s" % (settings["DOMAIN_HOSTNAME"], settings["DOMAIN_NAME"])

    settings["CM_USER"] = CM_USER

    settings["IP_ADDRESS_GATEWAY_1"] = IP_ADDRESS_GATEWAY_1
    settings["IP_ADDRESS_GATEWAY_2"] = IP_ADDRESS_GATEWAY_2
    settings["IP_ADDRESS_GATEWAY_3"] = IP_ADDRESS_GATEWAY_3
    settings["IP_ADDRESS_GATEWAY_4"] = IP_ADDRESS_GATEWAY_4
    settings["IP_ADDRESS_CM"] = IP_ADDRESS_CM

    settings["HOSTNAME_GATEWAY"] = HOSTNAME_GATEWAY
    settings["HOSTNAME_CM"] = HOSTNAME_CM

    download_file(
        "%s/bootstrap/%s/template.sh" % (settings["FORGE_BASE_URL"], settings["DOMAIN_HOSTNAME"]),
        "/tmp/template.sh")
    os.chmod("/tmp/template.sh", 0o755)

    for key, value in settings.items():
        sed_inplace("/tmp/template.sh", "__%s__" % key, value)

    print("")
