#!/usr/bin/env python3.8

import common

def init_settings(environment):
    settings = {}

    settings["ENV_DNS_FORWARDING"] = "false"

    if environment == "vm_dns_forwarding":
        environment = "vm"
        settings["ENV_DNS_FORWARDING"] = "true"

    settings["DOMAIN_HOSTNAME"] = common.HOSTNAME_GATEWAY
    settings["ENVIRONMENT"] = environment

    settings["DNS_ACCESS_CONTROL_NETBLOCK"] = "10.1.1.0\\/24"

    settings["SSH_ALLOW_USERS"] = "%s@%s" % (common.CM_USER, common.IP_ADDRESS_CM)

    settings["DNS_NUMBER_THREADS"] = "1"
    settings["DNS_NUMBER_SLABS"] = "2"
    settings["DNS_RRSET_CACHE_SIZE"] = "64m"
    settings["DNS_MSG_CACHE_SIZE"] = "32m"

    # Took it from /etc/master.passwd after creating a dumb user with the wanted password (test).
    settings["CM_PASSWORD"] = "\\\\$2b\\\\$10\\\\$a4ThpqcpGbdx8jOHUOtChu9RvQ6X5Yi4Wt5saRp1oFkcGkKjEbQ/C"

    if environment == "vm":
        settings["NIC_INTERFACE_NAME_1"] = "em0"
        settings["NIC_INTERFACE_NAME_2"] = "em1"
        settings["NIC_INTERFACE_NAME_3"] = "em2"
        settings["NIC_INTERFACE_NAME_4"] = "em3"

        settings["MAC_ADDRESS_GATEWAY_1"] = "00:00:10:00:00:01"
        settings["MAC_ADDRESS_GATEWAY_2"] = "00:00:10:01:01:01"
        settings["MAC_ADDRESS_GATEWAY_3"] = "00:00:10:02:02:01"
        settings["MAC_ADDRESS_GATEWAY_4"] = "00:00:10:10:10:01"
        settings["MAC_ADDRESS_CM"] = "00:00:10:01:01:05"

    if environment == "metal":
        settings["NIC_INTERFACE_NAME_1"] = "igb0"
        settings["NIC_INTERFACE_NAME_2"] = "igb1"
        settings["NIC_INTERFACE_NAME_3"] = "igb2"
        settings["NIC_INTERFACE_NAME_4"] = "igb3"

        settings["MAC_ADDRESS_GATEWAY_1"] = "0c:c4:7a:7f:82:78"
        settings["MAC_ADDRESS_GATEWAY_2"] = "0c:c4:7a:7f:82:79"
        settings["MAC_ADDRESS_GATEWAY_3"] = "0c:c4:7a:7f:82:7a"
        settings["MAC_ADDRESS_GATEWAY_4"] = "0c:c4:7a:7f:82:7b"
        settings["MAC_ADDRESS_CM"] = "b8:27:eb:83:60:ca"

    return settings

if __name__ == "__main__":
    args = common.parse_args()
    common.run(init_settings(args.environment))
