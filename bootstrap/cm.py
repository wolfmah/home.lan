#!/usr/bin/env python3

import common
import crypt

def init_settings(environment):
    settings = {}

    settings["ENV_DNS_FORWARDING"] = "false"

    if environment == "vm_dns_forwarding":
        environment = "vm"
        settings["ENV_DNS_FORWARDING"] = "true"

    settings["DOMAIN_HOSTNAME"] = common.HOSTNAME_CM
    settings["ENVIRONMENT"] = environment

    # Far from being secure, but I don't care for the VM...
    # import crypt; crypt.crypt("test", "cm")
    settings["CM_PASSWORD"] = "cmgYwzZI1eHeE"

    if environment == "vm":
        print("No custom values")

    if environment == "metal":
        print("No custom values")

        print("Not implemented yet!")
        exit(1)

    return settings

if __name__ == "__main__":
    args = common.parse_args()
    common.run(init_settings(args.environment))
