Notes and specifications on what the future of `home.lan` reserve. The order seen below is suppose to be the order of priority for development, unless specifically noted.

[[_TOC_]]


# Legend

SSH keys naming structure: `{user}--{origin}--{destination}`

Example: `cm--cm--gateway`

Enable the server `cm` (origin) to connect to the server `gateway` (destination), via the user `cm` (user).


# From scratch

When building this infrastructure from scratch, the configuration management (CM) can't be the first server built, it will need a gateway in front. If the gateway doesn't exist, a minimal functioning gateway needs to be setup, and then the CM can be created and manage the gateway's full configurations afterward.


## No auto-install

The way the CM server work, it can't manage uninstalled system; it requires a privileged user and a working SSH server.

So first, each server needs to be installed manually with a minimal SSH server working and a user that the CM server can use. The way I see it, first, I'll need to install the gateway, so to have DHCP on the network, then the CM, then the others.

Building from scratch is kind of weird, but it's not meant to happen often (except during development stage). Still, the process should be documented.


# Infrastructure


## IoT


### Managed switch (LAN)

Managed switch, on LAN network, used to plug all the necessary devices together.


#### Hardware

| Components | |
------ | ------
**Case** | [ProCurve-1800-24G (J9028B)](https://support.hpe.com/hpesc/public/docDisplay?docId=c01955915&docLocale=en_US)


### Wireless router

Allow the use of mobile device throughout the house.


#### Hardware

| Components | |
------ | ------
**Case** | [TP-Link Archer AX50 v1 (AX3000)](https://www.tp-link.com/ca/home-networking/wifi-router/archer-ax50/)
**Case** | [TP-Link Archer C7 v2 (AC1750)](https://www.tp-link.com/us/home-networking/wifi-router/archer-c7/)


### VoIP

Allow the use of traditional phones thoughout the house.


#### Hardware

| Components | |
------ | ------
**Case** | [OBiTalk OBi200](https://www.obitalk.com/info/products/obi200)


### Printer


#### Hardware

| Components | |
------ | ------
**Case** | [Epson WorkForce Pro WF-7820](https://epson.ca/For-Work/Printers/Inkjet/WorkForce-Pro-WF-7820-Wireless-Wide-format-All-in-One-Printer/p/C11CH78201)


### TV box


#### Hardware

| Components | |
------ | ------
**Case** | [nVidia Shield TV Pro](https://www.nvidia.com/en-us/shield/shield-tv-pro/)


## Common to all the hosts

* [netdata](https://docs.netdata.cloud/database/#running-a-dedicated-central-netdata-server)
  * For now, stick to only having netdata running with a dedicated central instance aggregating all the small ones.
  * Once, it's done, and the infrastructure have sufficiently progressed, think about migrating the metrics, from that one master instance, to another system (most probably Prometheus), so it can be visualized by Grafana.
* [fail2ban](), except the gateway (? not sure because of `pf-badhost`).


## Gateway

Server that will have multiple purposes: point of entry for the internet, packet filtering, NAT, DHCP, NTP, DNS, ad-blocker, remote access to local network.

Since it will be the only computer facing the open internet, it is imperative to have paramount security.


### Hardware

| Components | |
------ | ------
**Motherboard** | [SuperMicro X11SBA-LN4F](https://www.supermicro.com/en/products/motherboard/X11SBA-LN4F)
**Memory** | 2x Crucial 4GB DDR3L 1600 MHz SODIMM Kit
**Case** | [SuperMicro CSE-101S](https://www.supermicro.com/en/products/chassis/Mini-ITX/101/SC101S)


### Software

| **Hostname** | `gateway` |
------ | ------
**OS** | [OpenBSD](https://www.openbsd.org/)
**Firewall** | [pf](https://man.openbsd.org/pf) <br/> [pf-badhost](https://geoghegan.ca/pfbadhost.html)
**DHCP** | [dhcpd](https://man.openbsd.org/dhcpd)
**NTP** | [ntpd](https://man.openbsd.org/ntpd)
**DNS** | [unbound](https://man.openbsd.org/unbound) <br/> [unbound-adblock](https://geoghegan.ca/unbound-adblock.html) <br/> [Calomel config](https://calomel.org/unbound_dns.html)
**Remote access** | [OpenSSH](https://man.openbsd.org/sshd)
**VPN** | [WireGuard](https://blog.jasper.la/wireguard-on-openbsd.html)


## Configuration management (CM)

Will be the configuration management (CM) server, the one at the helm, governing the configurations of all non-mobile hosts on the network.

This server will pull Ansible playbooks from `git`, then execute them, to configure the hosts.


### Hardware

| Components | |
------ | ------
**Board** | [Raspberry Pi 3 Model B+](https://en.wikipedia.org/wiki/Raspberry_Pi)


### Software

| **Hostname** | `cm` |
------ | ------
**OS** | [Debian](https://www.debian.org/)
**Configuration management** | [Ansible](https://github.com/ansible/ansible)


## File server


### Hardware

| Components | |
------ | ------
**CPU** | [Intel Pentium G4560](https://ark.intel.com/content/www/us/en/ark/products/97143/intel-pentium-processor-g4560-3m-cache-3-50-ghz.html)
**Heatskink** | [ARCTIC Alpine 12 Passive](https://www.arctic.ac/us_en/alpine-12-passive.html)
**Motherboard** | [Supermicro X11SSM-F](https://www.supermicro.com/en/products/motherboard/X11SSM-F)
**Memory** | (32GB) 2x Kingston 16GB DDR4-2400 CL17 ECC Unbuffered
**PSU** | [Seasonic Focus Plus Series 550w 80+ Gold](https://seasonic.com/focus-plus-gold)
**Controller Card** | LSI 9211-8I SAS
**Hard drives** | ???


### Software

| **Hostname** | `nas` |
------ | ------
**OS** | [FreeNas](https://www.freenas.org/) **or** [ZoL on Debian](https://github.com/zfsonlinux/zfs/wiki/Debian)


## Source code management (SCM)

* SourceHut


## Kubernetes cluster

k3os
https://github.com/nrobinaubertin/dockerfiles (Firefox Sync and Radical docker container)


## Monitoring server


### Hardware

*Non-existent*


### Software

| **Hostname** | `monitoring` |
------ | ------
**OS** | ???

*Aggregation of Netdata, Prometheus, PromTail, Loki, Grafana*