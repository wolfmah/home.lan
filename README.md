# home.lan

This is a collection of documentation, scripts and Ansible playbooks about the configuration management for maintaining my home network of servers, desktops and laptops (phones and tablets would be great, but, as of this writing, iOS and Android are not good at that. I'm waiting patiently for some badass Linux distros to be ready). The vast majority of the infrastructure as code is within [Ansible](https://docs.ansible.com/) playbooks.

[[_TOC_]]

**Note**: None of the configuration done here are meant to scale beyond one host per role, i.e.: the group `gateway` only have one host, same goes for `nas` or whatever. For my home network, I don't need that level of complexity. Which means, the group `gateway` have one host: `gateway`, and only that one. Though, I will still define everything by their roles instead of their hostname.

All internal tests (development/staging) are done via manually building VMs. The usage of [Packer](https://github.com/hashicorp/packer) and [Vagrant](https://github.com/hashicorp/vagrant/) have been considered, and would be awesome in automating that process, but for now, it only add complexity that I don't need. Once the playbooks have prove their worth in production, I may look into them, as a curiosity project.


## Infrastructure

A detailed explanation of the infrastructure can be found in [`INFRASTRUCTURE.md`](INFRASTRUCTURE.md).


## Bootstrap

A detailed explanation of the bootstrapping can be found in [`bootstrap/README.md`](bootstrap/README.md).


## Annex

### Hard drives

This is mostly in relation to the following Ansible roles: `fstab`, `snapraid`, `smartd`.

For the server `nas`, I use a strict nomenclature. Parity drives are named `PARITY_#` and data drives are named `CONTENT_#`. The `#` is a sequential number, starting at 1 for both.

#### Partitioning

The following are manual steps to be performed when a new drive is to be added to `nas`.

1. Start `parted` as follows:

        sudo parted /dev/sdX

1. Create a new GPT `disklabel` (aka partition table):

       (parted) mklabel gpt

1. Set the default unit to TB:

       (parted) unit TB

1. Create one partition occupying all the space on the drive:

       (parted) mkpart primary 0 0

1. Check that the results are correct:

       (parted) print

1. Save and quit `parted`:

       (parted) quit

#### Formatting

To format the new partition as `ext4` file system:

    sudo mke2fs -t ext4 -L PARITY_1 -U 12345678-90ab-cdef-1234-5678900000## /dev/sdX#

I use a manual UUID to more easily differentiate between the drives. The `##` is a sequential number to be incremented to each new drive. This helps with the testing in virtual environment vs production.

### What I learned

During my time working with Ansible and provisioning/maintaining my servers, this is a list of all the stuff that I have touched on, most are new.

* Ansible
    * YAML
    * Jinja2
    * Python 3.x
    * Installation
        * How to download and compile the latest Python 3.x.
        * How to install a Python virtual environment (venv) from the newly compile Python 3.x.
        * How to install PIP modules to the Python venv.
        * How to download and compile the latest Ansible.
        * How to install Ansible inside the Python venv.
        * How to execute Ansible via the Python 3.x venv instead of the default Python 2.x found on the system.
        * All the above, in the user space, not via root.
        * All the above scripted via a shell script (`bash`, `ksh`, POSIX).
* SSH
    * Configuration 
    * Keys management
* OpenBSD
    * Everything is familiar to GNU/Linux, yet nothing works the same.
    * Low resources on the web; FreeBSD is WAY more popular, yet not the same.
    * How to configure:
        * `doas` (the new `sudo`)
        * `pf` (packet filtering rules; `iptables`)
        * `unbound` (dns resolver)
        * `dhcpd`/`dhclient` (dhcp server/client configuration)
        * `ntf` (time server configuration)
        * Manage multiple NICs
* Debian
    * How to manage Debian GNU/Linux distro, which is different enough to Ubuntu, another distro I know well.
* `nut`
    * UPS management across multiple servers.
    * How to install the master on OpenBSD.
    * How to configure all the slaves installed on other OS.
    * Everything properly secured via specific users/passwords.
* User management
    * How to generate password safely via Ansible.
        * How to store plain text password in Ansible Vault.
        * How to generate a proper hash from the plain text password; very different methods used for Debian and OpenBSD.
        * How to make all generated password idempotent via Ansible without writing them to disk.
* Samba server/shares
* Monitoring
    * Hard drive health
* SnapRAID
    * Software raid for managing lots of data on hard drives.
* Plex
    * How to automate the installation and upgrade process (which is a highly manual process with no thought on automation).
