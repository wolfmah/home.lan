#!/usr/bin/env bash
set -eu

###############################################################################
# Bash specific
# Get absolut path of the current script directory.
SOURCE="${BASH_SOURCE[0]}"
# Resolve $SOURCE until the file is no longer a symlink.
while [ -h "${SOURCE}" ]; do
    DIR="$( cd -P "$( dirname "${SOURCE}" )" >/dev/null && pwd )"
    SOURCE="$(readlink "${SOURCE}")"
    # If $SOURCE was a relative symlink, we need to resolve it relative to the
    # path where the symlink file was located.
    [[ ${SOURCE} != /* ]] && SOURCE="${DIR}/${SOURCE}"
done
DIR="$( cd -P "$( dirname "${SOURCE}" )" >/dev/null && pwd )"
###############################################################################

DIR_PARENT=$(dirname "${DIR}")
ORIGINAL_CWD=$(pwd)

SSH_KEY_NAMES="\
    backup_nas--nas--forge \
    backup_nas--nas--workstation \
    backup_nas--nas--gateway \
    cm--cm--nas \
    cm--cm--forge \
    cm--cm--workstation \
    cm--cm--gateway \
    wolfmah--workstation--nas \
    wolfmah--workstation--cm \
    wolfmah--workstation--forge \
    wolfmah--workstation--gateway"

# Generate the SSH keys.
cd "${DIR_PARENT}/ansible/roles/ssh-keys/files/"
rm -rf *
mkdir authorized_keys
for KEY_NAME in ${SSH_KEY_NAMES}; do
    echo "[INFO] Generate the SSH key ${KEY_NAME}..."

    # -o
    #   save private keys using the new OpenSSH format
    # -a
    #   number of rounds. useless since no passphrase is set
    # -t
    #   EdDSA
    # -q
    #   quiet
    # -N
    #   passphrase
    # -C
    #   comment
    # -f
    #   filename of the key file
    ssh-keygen -o -a 256 -t ed25519 -q -N '' -C "${KEY_NAME}" -f ${KEY_NAME}
done

# Fill the authorized_keys files.
echo "[INFO] Fill the authorized_keys files..."
for KEY_NAME in ${SSH_KEY_NAMES}; do
    # The last bit (##*-) is a bash trick to substring from the last match (in
    # this case -) to the end of the string.
    # Ex.: backup_nas--nas--forge will result in forge
    cat ${KEY_NAME}.pub >> authorized_keys/${KEY_NAME##*-}
done

cd "${DIR_PARENT}/ansible"
# Encrypt the SSH private keys.
for KEY_NAME in ${SSH_KEY_NAMES}; do
    echo "[INFO] Encrypt the SSH private key ${KEY_NAME}..."
    ansible-vault encrypt "${DIR_PARENT}/ansible/roles/ssh-keys/files/${KEY_NAME}"
done

cd "${ORIGINAL_CWD}"
