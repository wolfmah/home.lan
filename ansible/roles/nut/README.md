# Purpose
Install and configure Network UPS Tools (NUT).

## OS
- OpenBSD 6.9
- Debian 10

# Testing
The master is on OpenBSD, and the slaves will be on anything else.

## ... the USB detection
Test that the plugged USB UPS is detected by the system.

    (Debian)  # lsusb
    (OpenBSD) # usbdevs
 
Look for the word "UPS" or the manufacturer's name in the output.

## ... the USB driver
Once the configuration file `ups.conf` is complete, test the USB driver.

    # upsdrvctl start

Make sure the driver doesn’t report any errors. It should show a few details about the hardware and then enter the background. You should get back to the command prompt a few seconds later. For reference, a successful start of the `usbhid-ups` driver looks like this:

    # upsdrvctl start
    Network UPS Tools - Generic HID driver 0.34 (2.4.1)
    USB communication driver 0.31
    Using subdriver: MGE HID 1.12
    Detected EATON - Ellipse MAX 1100 [ADKK22008]

## ... the NUT server
Once the configuration files `upsd.conf` and `upsd.users` are complete, test the NUT server.

    # upsd

Make sure it is able to connect to the driver(s) on your system. A successful run looks like this:

    # upsd
    Network UPS Tools upsd 2.4.1
    listening on 127.0.0.1 port 3493
    listening on ::1 port 3493
    Connected to UPS [eaton]: usbhid-ups-eaton

## ... the UPS data
With the NUT server running, check the UPS data.

    # upsc <ups name from ups.conf> ups.status

You should see just one line in response:

    OL

## ... the shutdown
The first step is to see how `upsdrvctl` will behave without actually turning off power. To do so, use the `-t` argument:

    # upsdrvctl -t shutdown

It will display the sequence without actually calling the drivers.

You can finally test a forced shutdown sequence (FSD) using:

    # upsmon -c fsd

If everything works correctly, the computer will be forcibly powered off, may remain off for a few seconds to a few minutes (depending on the driver and UPS type), then will power on again.
