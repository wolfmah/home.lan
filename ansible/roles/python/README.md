# Purpose
Install and update the Python 3.x and PIP.

PIP modules installed:
- `passlib`
- `bcrypt`
- `cryptography`

## OS
- OpenBSD 6.9
- Debian 10
