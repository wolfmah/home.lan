# Purpose
Create a file `/tmp/ansible-vars` on the target system containing the relevant Ansible variables available to that host. Useful for debugging.
