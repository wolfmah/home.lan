# Purpose
Deploy a pre-configured unbound server on the host.

## OS
- OpenBSD 6.9

## Ref
https://calomel.org/unbound_dns.html
