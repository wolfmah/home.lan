# Purpose
Deploy a pre-configured DHCP server, or client, on the host.

## OS
- OpenBSD 6.9 (for the actual DHCP server)
- Debian 10 (for the dhclient)
