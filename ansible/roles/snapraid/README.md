# Purpose
Install and configure the entire hard drive stack/management.
* The RAID software: [SnapRAID](https://github.com/amadvance/snapraid/)
* The hard drive pool: [media_symbolic_links.sh](templates/media_symbolic_links.sh)
* `cron` jobs to automate SnapRAID

## OS
- Debian 10

## SnapRAID configuration reasoning
I have considered and tried `mhddfs` and `aufs` as a pool solution (merging multiple path into one). As a read-only stuff, it's good, though the one in SnapRaid (pool) already does this pretty easily (SnapRaid pool is read-only, no write). It's when writing stuff that it gets funky for my taste. Usually, when using true RAID, I don't care where my stuff end up; you can't decide anyway. But for SnapRaid, since all the data on the drives are readable outside of SnapRaid, I would like to keep it somewhat coherent; I don't want a TV series spread out across 5 drives if I have to dig in myself, or worse, my pictures... What I would like is for the pool system to prioritize the creation of content in the drive where the folder reside first, but I just couldn't make it work with either `mhddfs` or `aufs` (UPDATE: might be worth considering [`mergerfs`](https://github.com/trapexit/mergerfs)). So I said "fuck it", since I have big drives (8TB and 10TB), I can manage a pool of data with symbolic link pretty easily. Eventually, if I phase out the 8TB and replace them with 14TB, it will become even easier to manage the pool.
