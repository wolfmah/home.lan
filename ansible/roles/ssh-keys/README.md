# Purpose
Copies the SSH keys used throughout `home.lan`.

## Generate new keys
```sh
cd {{ ROLE_PATH }}/files

# -t: Specifies the type of key to create.
# -a: Specifies the number of rounds (key derivation) to perform.
# -o: Causes ssh-keygen to save private keys using the new OpenSSH format rather than the more compatible PEM format.
# -N: Provides the new passphrase.
# -C: Provides a new comment.
# -f: Specifies the filename of the key file.
ssh-keygen -t ed25519 -a 100 -o -q -N "" -C "{{ KEY_NAME }}" -f ./{{ KEY_NAME }}

# Manually vault the private key
cd {{ PROJECT_ANSIBLE_ROOT }}
ansible-vault encrypt ./roles/ssh-keys/files/{{ KEY_NAME }}

# Manually combine the public key into the appropriate authorized_keys

# Add the relevant tasks to tasks/main.yml
```
