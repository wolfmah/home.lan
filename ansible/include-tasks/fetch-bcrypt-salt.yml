---
# Note on the password: OpenBSD only accept BCrypt hashes.
#
# First, generate a BCrypt compatible salt (blowfish in Ansible filter
# `password_hash` lingo). BCrypt expects a 128 bit (32 chars) salt encoded in a
# base64 format, while keeping only the first 22 characters of the resulting
# hash. Even the resulting base64 string needs to be manipulated as `+` char
# needs to be converted to `.` char to fit into the regexp range
# `[./A-Za-z0-9]`.
#
# After some tests, Debian seems incapable of generating a proper salt, via
# `openssl`, `sed` and `cut`. Though it works fine when done on an OpenBSD
# machine. So, since the generated salt will be stored on the CM machine, and
# it's a Debian OS, I generate a salt via Python by generating a random
# password and extracting the 22 relevant characters from the resulting hash.
# 
# Once generated, the salt is written in the playbook directory. It is quite
# safe as it have default permissions of 0600.
#
# If the file already exists, it won't regenerate a new salt, it will read
# from the file instead, making all this idempotent.
#
# Reference: https://blog.ircmaxell.com/2012/12/seven-ways-to-screw-up-bcrypt.html
#            https://passlib.readthedocs.io/en/stable/lib/passlib.hash.bcrypt.html
- name: Fetch BCrypt salt
  delegate_to: localhost
  become: true
  block:
    - name: Set bcrypt_salt_file
      ansible.builtin.set_fact:
        bcrypt_salt_file: "{{ playbook_dir }}/.openbsd-password-salt-{{ inventory_hostname }}"
      changed_when: false

    - name: Does BCrypt salt file exists
      ansible.builtin.stat:
        path: "{{ bcrypt_salt_file }}"
      register: stat_salt
      changed_when: false

    - name: BCrypt salt file exists
      when: stat_salt.stat.exists == true
      block:
        - name: Read BCrypt salt
          ansible.builtin.set_fact:
            bcrypt_salt_read: "{{ lookup('file', bcrypt_salt_file) }}"
          changed_when: false

    - name: BCrypt salt file doesn't exists
      when: stat_salt.stat.exists == false
      block:
        - name: Generate BCrypt salt
          script: "{{ playbook_dir }}/include-tasks/files/generate-bcrypt-salt.py"
          args:
            executable: "{{ ansible_playbook_python }}"
          register: script_bcrypt_salt_generated
          changed_when: false

        - name: Get primary group of current Ansible user
          command:
            cmd: "id -g -n {{ ansible_user_id }}"
          register: command_ansible_user_group
          changed_when: false

        - name: Write BCrypt salt
          ansible.builtin.copy:
            content: "{{ script_bcrypt_salt_generated.stdout }}"
            dest: "{{ bcrypt_salt_file }}"
            mode: 0600
            owner: "{{ ansible_user }}"
            group: "{{ command_ansible_user_group.stdout }}"
